package api_service::Controller::User::Delete;
use Mojo::Base 'Mojolicious::Controller', -signatures;

sub delete_user ($self) {

  $self->render(json => [
      { status => 'ok' }
  ]);

}

1;
