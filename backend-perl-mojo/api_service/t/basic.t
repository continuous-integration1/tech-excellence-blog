use Mojo::Base -strict;

use Test::More;
use Test::Mojo;

my $t = Test::Mojo->new('api_service');
$t->get_ok('/auth/status')->status_is(200)->json_is([{status => 'ok'}]);

done_testing();
