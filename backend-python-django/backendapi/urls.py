from django.urls import path, include
from blog import views

urlpatterns = [
    path('api/blog/', include('blog.urls')),
]
