from rest_framework import serializers
from .models import Post


class PostSerializer(serializers.ModelSerializer):
    created = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S")
    edited = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S")
    class Meta:
        model = Post
        fields = ['title','author','created','edited','text']
