from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Post
from .serializers import PostSerializer

import uuid


class PostList(APIView):
    """
    List all posts.
    """
    def get(self, request):
        posts = Post.objects.all()
        serializer = PostSerializer(posts, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

class PostCreate(APIView):
    """
    Add post.
    """
    def post(self, request):

        post = Post.objects.create(title= str(uuid.uuid4()) )
        post.save()
        serializer = PostSerializer(post)
        return Response(serializer.data, status=status.HTTP_200_OK)
