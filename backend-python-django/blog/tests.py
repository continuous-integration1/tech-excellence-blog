from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from blog.models import Post


class PostsTests(TestCase):

    post: Post

    def setUp(self):

        self.post = Post.objects.create(title="title", author="author", text="some text")


    def test_get_list(self):
        response = self.client.get(
            reverse('get_posts_list'),
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data[0]['title'], 'title')
        self.assertEqual(response.data[0]['author'], 'author')
        self.assertEqual(response.data[0]['text'], 'some text')
        self.assertEqual(response.data[0]['created'], self.post.created.strftime("%Y-%m-%d %H:%M:%S"))

    def test_post_add(self):
        response = self.client.post(
            reverse('post_add_post'),
        )

        self.assertEqual(response.data['edited'], self.post.created.strftime("%Y-%m-%d %H:%M:%S"))
