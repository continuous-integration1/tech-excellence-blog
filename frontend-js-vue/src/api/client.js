export default {
  getPosts: async () => {
    const response = await fetch("/api/blog/posts/?format=json");
    if (response.status === 200 || response.status === 201) {
      return response.json();
    }
    if (response.status === 400) {
      const json = await response.json();
      console.log(json.error);
      throw new Error(json.error);
    }
    throw new Error(`Something went wrong: error ${response.status}`);
  },
  getStatus: async () => {
    const response = await fetch("/auth/status");
    return response.ok ? "ok" : "not ok";
  },
  createPost: async ( post ) => {
    const response = await fetch("/api/blog/posts/create", {
      method : 'POST',
      headers: {
        'Content-Type': 'application/json;charset=utf-8'
      },
      body : JSON.stringify(post),
    });
    return response.ok;
  },
};
